# elk-parser

## Main file

The main file is INFO.OUT. The parsing starts from this one and it fails if it is not present.

## Other files coming from a ground-state calculation

 The following files must be in the same folder as INFO.OUT:

 * TDOS.OUT: density of states.
 * EIGVAL.OUT: eigenvalues.

